library(shiny)
library(shinyBS)
library(shinydashboard)
library(shinyjs)
library (mrgsolve)
library (dplyr)
library (purrr)
library (ggplot2)
library (parallel)
library (DT)
library (tidyr)
library (flux)
library (plotly)
library (stringr)
library(testthat)

# library finish here

ui_dashboardBody <- dashboardBody(tags$head(
  tags$link(rel = "stylesheet", type = "text/css", href = "styles.css")),
  fluidPage(
    #actionButton("browser", "browser"),
    # verbatimTextOutput("testestest"),
    tabsetPanel(
    id = "wizard", type = "hidden",
    
    tabPanel("page_2",
             column (width = 4,
                     valueBoxUI ("model_selection_ModID",css_id = "model_selection_box_css"),
                    infoBoxUI ("fixed_effect_ModID",css_id = "fixed_effect_box_css", width = 6),
                    infoBoxUI ("random_effect_ModID",css_id = "random_effect_box_css", width = 6),
                    infoBoxUI ("sim_setting_ModID", css_id = "sim_setting_box_css")
                    ),
            column (width = 8,
                    overview_page2_UI()
                    ),
            
            ),
    
    tabPanel("page_1",
             tags$div (id = "result_resultPage_box_css",box (title = "Result", width = 8,resultPage1UI("resultPage1_ModID"))),
             tags$div (id = "plotOption_resultPage_box_css",
                       box (title = "Plotting Option",width = 2,
                            PlotTableOptionUI("PlotTableOption_ModID"))),
             tags$div (id = "SimList_resultPage_box_css",box (title = "Simulation List",width = 2,
                                    #htmlOutput("simListHtml"),
                                    simListUI("simList_ModID"),
                                    actionButton("switch_page_2","",icon = icon ("plus")))),
             tags$div (id = "Overview_resultPage_box_css",box (title = "overview",width = 4,verbatimTextOutput("check_review"))),
             
             ),
    
    ),
            modelSelectionUI ("model_selection_modal_modID"),
            fixedEffectUI ("fixed_effect_modal_modID"),
            randomEffectUI ("random_effect_modal_modID"),
            simSettUI ("sim_setting_modal_ModID")
))


ui <- tagList (useShinyjs(),
               title_logo(),
               dashboardPage(
                 dashboardHeader(title = "Viridian",disable = TRUE),
                 dashboardSidebar(disable = TRUE),
                 ui_dashboardBody))

shinyUI(ui)

