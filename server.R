server <- function(input, output, session) {
  ###################
  ### SWITCH PAGE ###
  ###################
  switch_page <- function(i) {
    updateTabsetPanel(inputId = "wizard", selected = paste0("page_", i))
  }
  observeEvent(input$switch_page_1, switch_page(1))
  observeEvent(input$switch_page_2, switch_page(2))
  observeEvent(page_switch(), {
    cat("\n\n modify page swith hit \n\n")
    switch_page(2)
    })
  
  ### Click Review the First Time ###
  o <- observe({
    req(mod())
    delay (3000, hide ("text"))
    shinyjs::click("submit_rx_syntax")
    o$destroy() # destroy observer as it has no use after initial button click
  })
  
  
  ### --- SETUP PAGE --- ###
  
  #########################
  ### RENDER CUSTOM BOX ###
  #########################
  
  model_selection_print <- reactive ({print_model_selection_fn(mod())}) # !!! changed 
  
  fixed_print <- reactive ({print_fixed_eff_fn(mod())}) # !!! changed 
  
  random_print <- reactive ({print_random_eff_fn(mod())}) # !!! changed 
  
  fixed_print_subtitle <- reactive ({print_fixed_eff_subtitle_fn(mod())}) # !!! changed 
  
  random_print_subtitle <- reactive ({print_random_eff_subtitle_fn(mod())}) # !!! changed 
  
  simsetting_print_li <- reactiveValues()
  
  simsetting_print <- reactive (print_simSetting_fn (reactiveValuesToList(simsetting_print_li)))
  
  valueBoxServer ("model_selection_ModID",
                 title = "Model",
                 value = model_selection_print,
                 color = "purple")
  
  infoBoxServer ("fixed_effect_ModID",
                 icon = "bars",
                 title = "Fixed Effect", 
                 value = fixed_print,
                 subtitle = fixed_print_subtitle,
                 color = "red")
  
  infoBoxServer ("random_effect_ModID",
                 icon = "shuffle",
                 title = "Random Effect",
                 value = random_print,
                 subtitle = random_print_subtitle,
                 color = "red")
  
  
  infoBoxServer ("sim_setting_ModID",
                 icon = "gear",
                 title = "Simulation Setting", 
                 value = simsetting_print,
                 color = "blue")
  
  ################################
  ### VALUE FOR EACH PARAMETER ###
  ################################
  modelSelectionList <- reactiveValues()
  timeGridList      <- reactiveValues()
  simSettList    <- reactiveValues()
  
  mod <- modelSelectionServer ("model_selection_modal_modID", 
                               curr_sim_no, modelSelectionList)
  fixedEffectListR <- fixedEffectServer ("fixed_effect_modal_modID", mod)
  randomEffectListR <- randomEffectServer ("random_effect_modal_modID", mod)
  simSettServer ("sim_setting_modal_ModID", simSettList, timeGridList, simsetting_print_li)
  
  observeEvent(input$bw_check, {
    modelSelectionList[["bw"]] <- bw_value_fn(input$bw_check)
  })
  
  
  ###################################
  ### Individual Simulation List ####
  ###################################
  
  overviewPage2Server ("overviewPage2_ModID",indiSimulation)
  
  indiSimulation <- reactiveValues ()
  
  observeEvent({input$submit_rx_syntax},{
    indiSimulation$model <- isolate (mod())
    indiSimulation$model_info <- reactiveValuesToList(modelSelectionList)
    indiSimulation$ev <- isolate (input$rx_syntax)
    indiSimulation$tgrid <- reactiveValuesToList(timeGridList)
    indiSimulation$simSett <- reactiveValuesToList(simSettList)
    indiSimulation$fixedEffect <- isolate (fixedEffectListR())
    indiSimulation$randomEffect <- isolate (randomEffectListR())
  })
  
  
  ############################
  ### ALL Simulation List ####
  ############################
  
  n_all_sims <- reactiveVal(c())
  curr_sim_no <- reactiveVal(1)
  
  allSimulation <- reactiveValues() # list of all raw param for each simulation
  
  allDataset_allSim <- reactiveValues() # list of all dataset for each simulation
  allSumstat_allSim <- reactiveValues() # list of all sumstat dataset for each simulation
  
  observeEvent({input$switch_page_1},{
    # updating individual Simulation 
    indiSimulation$model <- isolate (mod())
    indiSimulation$model_info <- reactiveValuesToList(modelSelectionList)
    indiSimulation$ev <- isolate (input$rx_syntax)
    indiSimulation$tgrid <- reactiveValuesToList(timeGridList)
    indiSimulation$simSett <- reactiveValuesToList(simSettList)
    indiSimulation$fixedEffect <- isolate (fixedEffectListR())
    indiSimulation$randomEffect <- isolate (randomEffectListR())
    
    simulation_name <- paste0 ("simulation_",curr_sim_no())
    allSimulation[[simulation_name]]$model  <- indiSimulation$model 
    allSimulation[[simulation_name]]$model_info  <- indiSimulation$model_info 
    allSimulation[[simulation_name]]$ev <- indiSimulation$ev 
    allSimulation[[simulation_name]]$tgrid <- indiSimulation$tgrid 
    allSimulation[[simulation_name]]$simSett <- indiSimulation$simSett
    allSimulation[[simulation_name]]$fixedEffect <- indiSimulation$fixedEffect 
    allSimulation[[simulation_name]]$randomEffect <- indiSimulation$randomEffect 
    
    df_indiSimulation <- dataset_indiSimulation_nsubj_nite (indiSimulation)
    allDataset_allSim[[simulation_name]] <- df_indiSimulation
    
    allSumstat_allSim[["cmax"]][[simulation_name]] <- cmax_tmax_each_sim_fn(df_indiSimulation,indiSimulation$ev)
    allSumstat_allSim[["cmin"]][[simulation_name]] <- cmin_tmin_each_sim_fn(df_indiSimulation,indiSimulation$ev)
    #allSumstat_allSim[["auc"]][[simulation_name]] <- auc_each_sim_fn()
    
    if (!(curr_sim_no() %in% n_all_sims())){
      n_all_sims (c(curr_sim_no(),n_all_sims())) # need if statement
    }
      
  })
  
  ### --- RESULT PAGE --- ###
  
  observeEvent({input$switch_page_2},{
    curr_sim_no(length (n_all_sims()) + 1)
  })
  
  ### List Review ###
  
  output$check_review<- renderPrint({
    reactiveValuesToList(allSimulation)
  })
  
  ### Simulation List Names ###
  
  page_switch <- reactiveVal(0)
  simListServer("simList_ModID", n_all_sims, curr_sim_no,page_switch)
  
  # output$simListHtml <- renderUI ({
  #   simulation_list_fn (curr_sim_no())
  # })
  
  ### Result and Setting Result ###
  
  dataset_combine <- eventReactive (reactiveValuesToList(allDataset_allSim),{
    binding_allDataset_allSim (reactiveValuesToList(allDataset_allSim))
  },ignoreInit = TRUE)
  
  
  option_result_connect <- reactiveValues(page = 1) # for talking between option and result module
  
  page_result_option <- reactiveVal(0)
  wrapper <- reactiveValues()
  
  resultPage1Server ("resultPage1_ModID",dataset_combine, allSumstat_allSim, page_result_option, wrapper)
  
  PlotTableOptionServer("PlotTableOption_ModID", dataset_combine, page_result_option, wrapper)
  
  observeEvent(input$browser, browser())
  
  # output$testestest <- renderPrint({
  #   cat ("\n\n n_all_sim  : ",n_all_sims(),
  #           "\n\n curr_sim_no  : ",curr_sim_no(),
  #           "\n\n")
  # })
  # 
  
}


shinyServer(server)