if(!require(renv)){
  print ("Installing Renv ...")
  install.packages("renv")
  library(renv)
}
if(!require(pkgbuild)){
  print ("Installing pkgbuild ...")
  install.packages("pkgbuild")
  library(pkgbuild)
}
if (pkgbuild::has_rtools() && pkgbuild:::rtools_path_is_set() && file.exists(pkgbuild::rtools_path())){
  renv::restore()
  shiny::runApp()
} else {
  cat (paste0 ("You need to install RTools.\n",
               "You have ", R.version.string,"\n",
               "Please download RTools4.2 for R version >=4.2.0\n",
               "Or RTools4.0 for R version 4.0.0 to 4.1.3\n",
               "To Download RTools Visit Website : https://cran.r-project.org/bin/windows/Rtools/"))
}