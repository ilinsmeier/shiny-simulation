# return dataframe of environment of model
ev_indiSimulation <- function (indiSimulation){
  ev_rx (indiSimulation$ev) %>% as.data.frame() %>% 
    mutate(amt = amt * indiSimulation$model_info$bw / indiSimulation$model_info$unit_conversion_y)
}

update_model_without_subj <- function (indiSimulation){
  indiSimulation$model %>% 
    ev(ev_unit_conversion (indiSimulation)) %>% 
    param (indiSimulation$fixedEffect) %>% 
    omat (indiSimulation$randomEffect %>% dmat ()) # !!! make sure it's on order
}

# return simulated dataframe of 1 subject 1 iteration
dataset_indiSimulation <- function (indiSimulation){
  mod_update <- update_model_without_subj(indiSimulation)
  mod_update %>% mrgsim (tgrid = do.call ("tgrid", indiSimulation$tgrid)) %>%
    as.data.frame() %>% mutate (C = C * indiSimulation$model_info$unit_conversion_y)
  
}

update_model_with_subj <- function (indiSimulation){
  indiSimulation$model %>% 
    ev(ev_unit_conversion_with_subj(indiSimulation)) %>% 
    param (indiSimulation$fixedEffect) %>% 
    omat (indiSimulation$randomEffect %>% dmat ()) # !!! make sure it's on order
}

# return simulated dataframe of x subject 1 iteration
dataset_indiSimulation_nsubj <- function (indiSimulation){
  mod_update <- update_model_with_subj(indiSimulation)
  mod_update %>% mrgsim (tgrid = do.call ("tgrid", indiSimulation$tgrid)) %>%
    as.data.frame() %>% mutate (C = C * indiSimulation$model_info$unit_conversion_y)
    
}

# return simulated dataframe of x subject y iteration for 1 simulation 
dataset_indiSimulation_nsubj_nite <- function (indiSimulation){
  set.seed (indiSimulation$simSett$seed)
  result <- parallel::mclapply(1:indiSimulation$simSett$num_ite, function(i) {
    dataset_indiSimulation_nsubj (indiSimulation) %>% 
      mutate(REPI = i)
  })%>% bind_rows(.) %>% mutate (SIM_NAME = indiSimulation$model_info$sim_name)
  return (result)
}

# binding row for each simulation (dataframe)
binding_allDataset_allSim <- function (allDataset_allSim){
  n <- allDataset_allSim %>% names () %>% length ()
  result <- parallel::mclapply(1:n, function(i) {
    allDataset_allSim[[paste0 ("simulation_",i)]]
  })%>% bind_rows(.)
  return (result)
}


# return simulated dataframe of x subject y iteration for multi simulation 
combine_dataset_indiSimulation_nsubj_nite <- function (allSimulation){
  n <- allSimulation %>% names () %>% length ()
  result <- parallel::mclapply(1:n, function(i) {
    dataset_indiSimulation_nsubj_nite (allSimulation[[paste0 ("simulation_",i)]]) %>% 
      mutate(SIM_NAME = i)
  })%>% bind_rows(.)
  return (result)
}


########## CONVERSION Y  #########
conversion_exist_fn <- function(mod){
  "CONV" %in% names (mod@fixed)
}

conversion_unit_fn <- function (mod){
  if (conversion_exist_fn(mod)){
    return (mod@fixed$CONV)
  } else {
    return (1)
  }
}

ev_unit_conversion <- function (indiSimulation){
  ev_rx (indiSimulation$ev) %>% as.data.frame() %>% 
    mutate(amt = amt * indiSimulation$model_info$bw / indiSimulation$model_info$unit_conversion_y)
}
  
ev_unit_conversion_with_subj <- function (indiSimulation){
  ev_rx (indiSimulation$ev) %>% 
    ev_rep (ID = 1:indiSimulation$simSett$num_subj) %>% 
    as.data.frame() %>% 
    mutate(amt = amt * indiSimulation$model_info$bw / indiSimulation$model_info$unit_conversion_y) 
}  
  
  
bw_value_fn <- function (val){
  if (val){
    return (85)
  } else {return (1)}
}  
  
  
  
  
  
  

